{
    "swagger": "2.0",
    "info": {
        "description": "This is a laundry server.",
        "version": "1.0.0",
        "title": "NL Laundry App",
        "termsOfService": "http://nl-laundry.com/terms/",
        "contact": {
            "email": "support@nl-laundry.com"
        }
    },
    "basePath": "/api",
    "tags": [
        {
            "name": "User",
            "description": "User APIs"
        },
        {
            "name": "Product",
            "description": "Product APIs"
        },
        {
            "name": "Cart",
            "description": "Cart APIs"
        }
    ],
    "schemes": [
        "http",
        "https"
    ],
    "paths": {
        "/v1/auth/users": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "find Users",
                "operationId": "User_find",
                "parameters": [
                    {
                        "name": "filter",
                        "description": "{ \"field\" : \"value\"}",
                        "in": "query",
                        "required": false,
                        "type": "string"
                    },
                    {
                        "name": "fields",
                        "description": "field_1 field_2 ...",
                        "in": "query",
                        "required": false,
                        "type": "string"
                    },
                    {
                        "name": "skip",
                        "in": "query",
                        "required": false,
                        "type": "string"
                    },
                    {
                        "name": "limit",
                        "in": "query",
                        "required": false,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            },
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "create a new User",
                "operationId": "User_create",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/User"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                }
            }
        },
        "/v1/auth/users/{id}": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "find a User by Id",
                "operationId": "User_findById",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            },
            "put": {
                "tags": [
                    "User"
                ],
                "summary": "update a User by Id",
                "operationId": "User_updateById",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserUpdate"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            },
            "delete": {
                "tags": [
                    "User"
                ],
                "summary": "delete a User by Id",
                "operationId": "User_deleteById",
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/activate-user": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "activate User account",
                "operationId": "User_activateUser",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserActivation"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                }
            }
        },
        "/v1/auth/user-activation-token": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "get User Activation Token",
                "operationId": "User_getUserActivationToken",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserActivationToken"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                }
            }
        },
        "/v1/auth/login": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "log a User in",
                "operationId": "User_login",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserLogin"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                }
            }
        },
        "/v1/auth/google/web/role/{selectedRole}": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "log a web User in via Google",
                "operationId": "User_googleWebAuth",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "selectedRole",
                        "in": "path",
                        "required": true,
                        "type": "string",
                        "value": "customer"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                }
            }
        },
        "/v1/auth/google/android/role/{selectedRole}": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "log an android User in via Google",
                "operationId": "User_googleAndroidAuth",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "selectedRole",
                        "in": "path",
                        "required": true,
                        "type": "string",
                        "value": "customer"
                    },
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserAndroidLogin"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                }
            }
        },
        "/v1/auth/google/android/connect/{id}": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "connect Google account with User's account",
                "operationId": "User_googleAndroidConnect",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserAndroidLogin"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/google/android/disconnect/{id}": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "disconnect Google account from User's account",
                "operationId": "User_googleAndroidDisconnect",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/validate-access-token": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "validate User Access Token",
                "operationId": "User_validateAccessToken",
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/logout": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "log a User out",
                "operationId": "User_logout",
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/forgot-password": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "forgot password request",
                "operationId": "User_forgotPassword",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserForgotPassword"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                }
            }
        },
        "/v1/auth/reset-password": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "reset User's password",
                "operationId": "User_resetPassword",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserResetPassword"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                }
            }
        },
        "/v1/auth/change-password/{id}": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "change User's password",
                "operationId": "User_changePassword",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "body",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/UserChangePassword"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/change-email/{id}": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "change User's Email",
                "operationId": "User_changeEmail",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "email",
                        "in": "formData",
                        "required": true,
                        "type": "string",
                        "value": "johndoe@gmail.com"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/activate-email/{id}": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "activate User's Email",
                "operationId": "User_activateEmail",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "email",
                        "in": "formData",
                        "required": true,
                        "type": "string",
                        "value": "johndoe@gmail.com"
                    },
                    {
                        "name": "emailToken",
                        "in": "formData",
                        "required": true,
                        "type": "string",
                        "value": "987123"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/email-activation-token/{id}": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "get Email Activation Token",
                "operationId": "User_getEmailActivationToken",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/change-phone/{id}": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "change User's Phone Number",
                "operationId": "User_changePhone",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "phoneNumber",
                        "in": "formData",
                        "required": true,
                        "type": "string",
                        "value": "9999911111"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/activate-phone/{id}": {
            "post": {
                "tags": [
                    "User"
                ],
                "summary": "activate User's Phone Number",
                "operationId": "User_activatePhone",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    },
                    {
                        "name": "phoneNumber",
                        "in": "formData",
                        "required": true,
                        "type": "string",
                        "value": "9999911111"
                    },
                    {
                        "name": "phoneToken",
                        "in": "formData",
                        "required": true,
                        "type": "string",
                        "value": "987123"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/auth/phone-activation-token/{id}": {
            "get": {
                "tags": [
                    "User"
                ],
                "summary": "get Phone Activation Token",
                "operationId": "User_getPhoneActivationToken",
                "produces": [
                    "application/json"
                ],
                "parameters": [
                    {
                        "name": "id",
                        "in": "path",
                        "required": true,
                        "type": "string"
                    }
                ],
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/salon-brands/all": {
            "get": {
                "tags": [
                    "SalonBrand"
                ],
                "summary": "find all Salon Brands",
                "operationId": "SalonBrand_findAll",
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/salon-depts/all": {
            "get": {
                "tags": [
                    "SalonDepartment"
                ],
                "summary": "find all Salon Departments",
                "operationId": "SalonDepartment_findAll",
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                },
                "security": [
                    {
                        "Bearer": []
                    }
                ]
            }
        },
        "/v1/salons/near-me": {
            "get": {
                "tags": [
                    "Salon"
                ],
                "summary": "find Salons near Me",
                "operationId": "Salon_findNearMe",
                "responses": {
                    "200": {
                        "description": "Success response"
                    }
                }
            }
        }
    },
    "securityDefinitions": {
        "Bearer": {
            "type": "apiKey",
            "name": "Authorization",
            "in": "header",
            "description": "Authorization header using the Bearer scheme"
        }
    },
    "definitions": {
        "User": {
            "type": "object",
            "properties": {
                "_id": {
                    "type": "string"
                },
                "phoneNumber": {
                    "type": "string",
                    "example": "9999911111"
                },
                "email": {
                    "type": "string",
                    "example": "johndoe@gmail.com"
                },
                "password": {
                    "type": "string",
                    "example": "John@123"
                },
                "confirmPassword": {
                    "type": "string",
                    "example": "John@123"
                },
                "firstName": {
                    "type": "string",
                    "example": "John"
                },
                "lastName": {
                    "type": "string",
                    "example": "Doe"
                },
                "gender": {
                    "type": "string",
                    "example": "male"
                },
                "selectedRole": {
                    "type": "string",
                    "example": "customer"
                }
            }
        },
        "UserUpdate": {
            "type": "object",
            "properties": {
                "firstName": {
                    "type": "string",
                    "example": "John"
                },
                "lastName": {
                    "type": "string",
                    "example": "Doe"
                },
                "gender": {
                    "type": "string",
                    "example": "female"
                }
            }
        },
        "UserActivation": {
            "type": "object",
            "properties": {
                "phoneNumber": {
                    "type": "string",
                    "example": "9999911111"
                },
                "phoneToken": {
                    "type": "string",
                    "example": "987123"
                },
                "email": {
                    "type": "string",
                    "example": "johndoe@gmail.com"
                },
                "emailToken": {
                    "type": "string",
                    "example": "987123"
                }
            }
        },
        "UserActivationToken": {
            "type": "object",
            "properties": {
                "phoneNumber": {
                    "type": "string",
                    "example": "9999911111"
                },
                "email": {
                    "type": "string",
                    "example": "johndoe@gmail.com"
                }
            }
        },
        "UserLogin": {
            "type": "object",
            "properties": {
                "phoneNumber": {
                    "type": "string",
                    "example": "9999911111"
                },
                "email": {
                    "type": "string",
                    "example": "johndoe@gmail.com"
                },
                "password": {
                    "type": "string",
                    "example": "John@123"
                }
            }
        },
        "UserAndroidLogin": {
            "type": "object",
            "properties": {
                "authCode": {
                    "type": "string"
                },
                "accessToken": {
                    "type": "string"
                },
                "refreshToken": {
                    "type": "string"
                }
            }
        },
        "UserForgotPassword": {
            "type": "object",
            "properties": {
                "phoneNumber": {
                    "type": "string",
                    "example": "9999911111"
                },
                "email": {
                    "type": "string",
                    "example": "johndoe@gmail.com"
                }
            }
        },
        "UserResetPassword": {
            "type": "object",
            "properties": {
                "token": {
                    "type": "string",
                    "example": "987123"
                },
                "newPassword": {
                    "type": "string",
                    "example": "John@1234"
                },
                "confirmPassword": {
                    "type": "string",
                    "example": "John@1234"
                }
            }
        },
        "UserChangePassword": {
            "type": "object",
            "properties": {
                "oldPassword": {
                    "type": "string",
                    "example": "John@123"
                },
                "newPassword": {
                    "type": "string",
                    "example": "John@1234"
                },
                "confirmPassword": {
                    "type": "string",
                    "example": "John@1234"
                }
            }
        },
        "SalonBrand": {
            "type": "object",
            "properties": {
                "_id": {
                    "type": "string"
                },
                "name": {
                    "type": "string",
                    "example": "Loreal"
                }
            }
        },
        "SalonDepartment": {
            "type": "object",
            "properties": {
                "_id": {
                    "type": "string"
                },
                "name": {
                    "type": "string",
                    "example": "Hair"
                },
                "unisexServices": {
                    "type": "array",
                    "example": [
                        "Cut",
                        "Step Cut",
                        "U Cut",
                        "V Cut"
                    ]
                },
                "maleServices": {
                    "type": "array",
                    "example": []
                },
                "femaleServices": {
                    "type": "array",
                    "example": []
                }
            }
        },
        "Salon": {
            "type": "object",
            "properties": {
                "_id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "slug": {
                    "type": "string"
                },
                "user": {
                    "type": "string"
                }
            }
        }
    }
}