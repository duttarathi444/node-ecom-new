//smsClient.js
//This code was posted for an article at https://codingislove.com/send-sms-developers/

const axios = require("axios");

const tlClient = axios.create({
    baseURL: "https://api.textlocal.in/",
    params: {
        apiKey: "RjLAnQSdPE0-si28Y61QbHgW2bOHXt7Bz0GubPwHDV", //Text local api key
        sender: "txtlcl"
    }
});

const smsClient = {
    sendPartnerWelcomeMessage: user => {
        if (user && user.phone && user.name) {
            const params = new URLSearchParams();
            params.append("numbers", [parseInt("91" + user.phone)]);
            params.append(
                "message",
                `Hi ${user.name},Welcome to i-estree`
            );
            tlClient.post("/send", params);
        }
    },
    sendVerificationMessage: user => {
        if (user && user.phone) {
            const params = new URLSearchParams();
            params.append("numbers", [parseInt("91" + user.phone)]);
            params.append(
                "message",
                `Your i-estree verification code is ${user.verifyCode}`
            );
            tlClient.post("/send", params);
        }
    }
};

module.exports = smsClient;

