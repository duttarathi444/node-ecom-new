const moment = require('moment');

const utcOffsetMins = 330;
const order_date_time = "08/01/2020 10:17 am";
var format = 'DD/MM/YYYY hh:mm a';
// const db_order_moment = moment(order_date_time, format).utc().utcOffset(utcOffsetMins);
// const cancel_order_moment = db_order_moment.clone().add(15, 'minutes');
const db_order_moment = moment.utc(order_date_time, format, true)
    .utcOffset(utcOffsetMins)
    .subtract(utcOffsetMins, 'minutes');
const cancel_order_moment = db_order_moment.clone().add(15, 'minutes');
const current_cancel_moment = moment().utc().utcOffset(utcOffsetMins);
// current_cancel_moment.second(0);
// current_cancel_moment.millisecond(0);

console.log("db_order_moment:- ", db_order_moment);
console.log("cancel_order_moment:- ", cancel_order_moment);
console.log("current_cancel_moment:- ", current_cancel_moment);

if (current_cancel_moment.isSameOrBefore(cancel_order_moment)) {
    console.log("\n\nCancelled");
} else {
    console.log("\n\nNot Cancelled");
}