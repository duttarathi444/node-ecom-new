// Import the top-level function of express
const express = require('express');
const https = require('https');
const checksum_lib = require('../lib/paytm/checksum');
// Creates an Express application using the top-level function
const app = express();
// Define port number as 3000
const port = 8000;
// Routes HTTP GET requests to the specified path "/" with the specified callback function
app.get('/', function (req, res) {
    // res.send('Hello, World!\n');
    console.log("url - get / =====");
    /* initialize an object with request parameters */
    var paytmParams = {

        /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
        //"MID": "zoTQbG62503921214041",
        "MID": "MAKSTE09405628161879",

        /* Find your WEBSITE in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
        "WEBSITE": "WEBSTAGING",

        /* Find your INDUSTRY_TYPE_ID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
        "INDUSTRY_TYPE_ID": "Retail",

        /* WEB for website and WAP for Mobile-websites or App */
        "CHANNEL_ID": "WEB",

        /* Enter your unique order id */
        "ORDER_ID": "1022",

        /* unique id that belongs to your customer */
        "CUST_ID": "2000",

        /* customer's mobile number */
        "MOBILE_NO": "9338468609",

        /* customer's email */
        "EMAIL": "durgabarik@gmail.com",

		/**
		* Amount in INR that is payble by customer
		* this should be numeric with optionally having two decimal points
		*/
        "TXN_AMOUNT": "1.00",

        /* on completion of transaction, we will send you the response on this URL */
        "CALLBACK_URL": "http://localhost:8000/paytm/callback",
    };
    console.log("paytmParams ======", JSON.stringify(paytmParams, 0, 2));
	/**
	* Generate checksum for parameters we have
	* Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
	*/
    //checksum_lib.genchecksum(paytmParams, "9k3kwpFqGWOwMNv3", function (err, checksum) {
    checksum_lib.genchecksum(paytmParams, "9x4hPx0VxwgJUYyC", function (err, checksum) {
        console.log("checksum ======", checksum);
        /* for Staging */
        var url = "https://securegw-stage.paytm.in/order/process";

        /* for Production */
        // var url = "https://securegw.paytm.in/order/process";

        /* Prepare HTML Form and Submit to Paytm */
        res.writeHead(200, { 'Content-Type': 'text/html' });
        res.write('<html>');
        res.write('<head>');
        res.write('<title>Merchant Checkout Page</title>');
        res.write('</head>');
        res.write('<body>');
        res.write('<center><h1>Please do not refresh this page...</h1></center>');
        res.write('<form method="post" action="' + url + '" name="paytm_form">');
        for (var x in paytmParams) {
            res.write('<input type="hidden" name="' + x + '" value="' + paytmParams[x] + '">');
        }
        res.write('<input type="hidden" name="CHECKSUMHASH" value="' + checksum + '">');
        res.write('</form>');
        res.write('<script type="text/javascript">');
        res.write('document.paytm_form.submit();');
        res.write('</script>');
        res.write('</body>');
        res.write('</html>');
        res.end();
    });
});

app.post('/paytm/callback', function (req, res) {
    console.log("url - post /paytm/callback =====");
    console.log(req.body);

    /* initialize an object */
    var paytmParams = {};

    /* Find your MID in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys */
    //paytmParams["MID"] = "zoTQbG62503921214041";
    paytmParams["MID"] = "MAKSTE09405628161879";

    /* Enter your order id which needs to be check status for */
    paytmParams["ORDERID"] = "1022";

    /**
    * Generate checksum by parameters we have
    * Find your Merchant Key in your Paytm Dashboard at https://dashboard.paytm.com/next/apikeys 
    */
    //checksum_lib.genchecksum(paytmParams, "9k3kwpFqGWOwMNv3", function (err, checksum) {
    checksum_lib.genchecksum(paytmParams, "9x4hPx0VxwgJUYyC", function (err, checksum) {

        /* put generated checksum value here */
        paytmParams["CHECKSUMHASH"] = checksum;

        /* prepare JSON string for request */
        var post_data = JSON.stringify(paytmParams);

        var options = {

            /* for Staging */
            hostname: 'securegw-stage.paytm.in',

            /* for Production */
            // hostname: 'securegw.paytm.in',

            port: 443,
            path: '/order/status',
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Content-Length': post_data.length
            }
        };

        // Set up the request
        var response = "";
        var post_req = https.request(options, function (post_res) {
            post_res.on('data', function (chunk) {
                response += chunk;
            });

            post_res.on('end', function () {
                console.log('Response: ', response);
                res.send(response);
            });
        });

        // post the data
        post_req.write(post_data);
        post_req.end();
    });
});
// Make the app listen on port 3000
app.listen(port, function () {
    console.log('Server listening on http://localhost:' + port);
});