'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const {
    cancelOrder,
    listOrderDetails,
    completedOrderList,
    createOrder3,
    orderTxnStatus,
    createMultipleOrders,
    recreateMultipleOrders,
    createMultipleOrders2,
    recreateMultipleOrders2,
    cancelMultipleOrder2,
    getCurrentDateTime,
    orderListById,
    orderDetailsById
} = require('../../controllers').OrderController;

const {
    getUserNotifications, getUserNotifications1, getDeliveryBoyNotifications, getLaundryManNotifications
    , pushAddhNotificationToCustomer, pushAppUpdateNotificationToCustomer
} = require('../../controllers').OrderNotificationsController;
const { authenticate, authorize, authorizeLaundrymanOrDeliveryBoy } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;
const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;

/**********************************************
 * ProductMaster Model APIs v1
 *
 *******************************/
router.get('/api/v1/order/getCurrentDateTime', catchErrors(getCurrentDateTime));
// router.post('/api/v1/order/createMultipleOrders/:user_id', authorize(), catchErrors(createMultipleOrders));
router.post('/api/v1/order/createMultipleOrders/:user_id', catchErrors(createMultipleOrders));
// BY COD (NEW)
router.post('/api/v1/order/recreateMultipleOrders/:user_id', authorize(), catchErrors(recreateMultipleOrders));
router.post('/api/v1/order/createMultipleOrders2/:user_id', authorize(), catchErrors(createMultipleOrders2));// BY ONLINE (NEW)
router.post('/api/v1/order/recreateMultipleOrders2/:user_id', authorize(), catchErrors(recreateMultipleOrders2));
router.post('/api/v1/order/createOrder3', catchErrors(createOrder3));
router.post('/api/v1/order/cancelOrder', authorize(), catchErrors(cancelOrder)); // CANCEL SINGLE ORDER FOR COD -OLD
router.post('/api/v1/order/cancelMultipleOrder2', authorize(), catchErrors(cancelMultipleOrder2)); // CANCEL SINGLE/MULTIPLE ORDER FOR ONLINE - NEW
router.post('/api/v1/paytm/orderTxnStatus', catchErrors(orderTxnStatus)); // Paytm Status Check - NEW
router.get('/api/v1/order/listOrderDetails/:user_id', catchErrors(listOrderDetails));
router.post('/api/v1/order/completedOrderList', catchErrors(completedOrderList));
router.post('/api/v1/order/getUserNotifications', authorize(), catchErrors(getUserNotifications));
router.post('/api/v1/order/getDeliveryBoyNotifications', authorizeLaundrymanOrDeliveryBoy(), catchErrors(getDeliveryBoyNotifications));
router.post('/api/v1/order/getLaundryManNotifications', authorizeLaundrymanOrDeliveryBoy(), catchErrors(getLaundryManNotifications));
router.post('/api/v1/order/pushAddhNotificationToCustomer', catchErrors(pushAddhNotificationToCustomer));
router.post('/api/v1/order/pushAppUpdateNotificationToCustomer', catchErrors(pushAppUpdateNotificationToCustomer));
router.post('/api/v1/order/getUserNotifications1', authorize(), catchErrors(getUserNotifications1));

router.post('/api/v1/order/orderListById', catchErrors(orderListById));

router.post('/api/v1/order/orderDetailsById', catchErrors(orderDetailsById));