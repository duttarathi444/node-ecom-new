'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const {
    googleAndroidAuth,
    logout,
    getProfile,
    editProfile,
    serviceAvailability,
    getAppVersion,
    checkAppVersion,
    userLogin
} = require('../../controllers').UserController;

const { authenticate, authorize } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;
const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;

/**********************************************
 * User Model APIs v1
 *
 *******************************/
// Get App Version details

router.get('/api/v1/getAppVersion', catchErrors(getAppVersion));
router.post('/api/v1/checkAppVersion', catchErrors(checkAppVersion));
// Google Android OAuth 2.0 Login API
router.post('/api/v1/auth/google/android/login', catchErrors(googleAndroidAuth));
router.post('/api/v1/auth/google/android/logout', catchErrors(logout));
router.post('/api/v1/user/getProfile', catchErrors(getProfile));
router.post('/api/v1/user/editProfile', catchErrors(editProfile));
router.post('/api/v1/user/serviceAvailability', catchErrors(serviceAvailability));

router.post('/api/v1/user/userLogin', catchErrors(userLogin));



