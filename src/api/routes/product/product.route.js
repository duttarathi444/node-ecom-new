'use strict';

// global imports
const express = require('express');
// local imports
const env = require('../../../env');
const {
    findCategory,
    findSubCategoryOrProductList,
    updateProductPrice,
    updateProductStock,
    productDetailsById
} = require('../../controllers').ProductController;

const { authenticate, authorize } = require('../../middlewares').AuthorizationMiddleware;
const { catchErrors } = require('../../middlewares').ErrorHandlerMiddleware;
const router = express.Router();
//const { v1 } = env.routes.user;

module.exports = router;

/**********************************************
 * ProductMaster Model APIs v1
 *
 *******************************/

router.get('/api/v1/product/findCategory', catchErrors(findCategory));
router.post('/api/v1/product/findSubCategoryOrProductList', catchErrors(findSubCategoryOrProductList));
router.post('/api/v1/product/updateProductPrice', catchErrors(updateProductPrice));
router.post('/api/v1/product/updateProductStock', catchErrors(updateProductStock));

router.post('/api/v1/product/productDetailsById', catchErrors(productDetailsById));