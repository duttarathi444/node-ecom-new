const Sequelize = require("sequelize");

const misUserLoginSchema = {
    schema: {
        // attributes
        id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        mis_user_id: {
            type: Sequelize.INTEGER
        },
        mis_user_login_id: {
            type: Sequelize.STRING,
            isAlphanumeric: true,
            len: [3, 10]
        },
        mis_user_password: {
            type: Sequelize.STRING
        },
        access_token: {
            type: Sequelize.STRING
        },
        mis_user_last_login_date_time: {
            type: Sequelize.STRING
        },
        mis_user_role_id: {
            type: Sequelize.INTEGER
        },
        fcmToken: {
            type: Sequelize.STRING
        },
        store_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = misUserLoginSchema;