const Sequelize = require("sequelize");

const employeeSchema = {
    schema: {
        // attributes
        emp_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        emp_fname: {
            type: Sequelize.STRING,
            isAlphanumeric: true,
            required: true,
            allowNull: false,
            len: [8, 20]
        },
        emp_lname: {
            type: Sequelize.STRING,
            required: true,
            allowNull: false
        },
        emp_email_id: {
            type: Sequelize.STRING,
            validate: {
                isEmail: {
                    msg: "Must be an email"
                }
            }
        },
        emp_age: {
            type: Sequelize.INTEGER
        },
        emp_gender: {
            type: Sequelize.STRING
        },
        emp_address_id: {
            type: Sequelize.INTEGER
        },
        emp_qualification: {
            type: Sequelize.STRING
        },
        emp_primary_id_type: {
            type: Sequelize.STRING
        },
        emp_primary_id_value: {
            type: Sequelize.STRING
        },
        emp_secondary_id_type: {
            type: Sequelize.STRING
        },
        emp_secondary_id_value: {
            type: Sequelize.STRING
        },
        emp_contact_no: {
            type: Sequelize.STRING
        },
        emp_contact_no2: {
            type: Sequelize.STRING
        },
        emp_bank_account_no: {
            type: Sequelize.STRING
        },
        ifsc_code: {
            type: Sequelize.STRING
        },
        emp_role_id: {
            type: Sequelize.INTEGER
        },
        emp_status: {
            type: Sequelize.STRING
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = employeeSchema;