const Sequelize = require('sequelize');

const roleMasterModel = {
    schema: {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        role_id: {
            type: Sequelize.INTEGER
        },
        role_name: {
            type: Sequelize.STRING
        }
    },
    options: {
        freezeTableName: true,
        timestamps: false
    }
}

module.exports = roleMasterModel;