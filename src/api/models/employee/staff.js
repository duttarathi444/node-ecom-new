const Sequelize = require('sequelize');

const staffModel = {
    schema: {
        staff_id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        staff_name: {
            type: Sequelize.STRING
        },
        staff_phoneno: {
            type: Sequelize.STRING
        },
        role_id: {
            type: Sequelize.INTEGER
        },
        store_id: {
            type: Sequelize.INTEGER
        },
        staff_status: {
            type: Sequelize.STRING
        }
    },
    options: {
        freezeTableName: true,
        timestamps: false
    }
}

module.exports = staffModel;