const Sequelize = require('sequelize');

const uomSchema = {
    schema: {
        id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        uom_desc: {
            type: Sequelize.STRING
        }
    },
    options: {
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = uomSchema;