const Sequelize = require("sequelize");

const storeSchema = {
    schema: {
        // attributes
        store_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        store_name: {
            type: Sequelize.STRING
        },
        store_description: {
            type: Sequelize.STRING
        },
        store_address: {
            type: Sequelize.STRING
        },
        flag: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = storeSchema;