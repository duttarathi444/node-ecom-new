const Sequelize = require("sequelize");

const productPriceSchema = {
    schema: {
        // attributes
        product_price_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        prod_id: {
            type: Sequelize.INTEGER
        },
        prod_price: {
            type: Sequelize.DECIMAL
        },
        prod_status: {
            type: Sequelize.STRING
        },
        vendor: {
            type: Sequelize.DECIMAL
        },
        delivery: {
            type: Sequelize.DECIMAL
        },
        company: {
            type: Sequelize.DECIMAL
        },
        store_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = productPriceSchema;