const Sequelize = require("sequelize");

const productMasterSchema = {
    schema: {
        // attributes
        prod_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        prod_code: {
            type: Sequelize.STRING
        },
        prod_name: {
            type: Sequelize.STRING
        },
        prod_desc: {
            type: Sequelize.STRING
        },
        barcode: {
            type: Sequelize.STRING
        },
        manufacturer_id: {
            type: Sequelize.INTEGER
        },
        sku: {
            type: Sequelize.STRING
        },
        sku_type: {
            type: Sequelize.STRING
        },
        prod_category_id: {
            type: Sequelize.INTEGER
        },
        parent_id: {
            type: Sequelize.INTEGER
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
        // classMethods: {
        //     associate: function (models) {
        //         User.belongsTo(models.User);
        //     }
        // },
        // instanceMethods: {
        //     sayTitle: function () {
        //         console.log(this.title)
        //     }
        // }
    }
}

module.exports = productMasterSchema;