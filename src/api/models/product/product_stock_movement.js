const Sequelize = require('sequelize');

const productStockMoveSchema = {
    schema: {
        stock_mov_id: {
            type: Sequelize.INTEGER,
            autoIncrement: true,
            primaryKey: true
        },
        store_id: {
            type: Sequelize.INTEGER,
            required: true
        },
        product_id: {
            type: Sequelize.INTEGER,
            required: true
        },
        mov_type: {
            type: Sequelize.INTEGER
        },
        mov_quantity: {
            type: Sequelize.INTEGER,
            required: true
        },
        mov_sku: {
            type: Sequelize.INTEGER
        },
        sales_order_id: {
            type: Sequelize.INTEGER
        },
        sales_date: {
            type: Sequelize.STRING
        },
        sales_time: {
            type: Sequelize.STRING
        },
        close_stock: {
            type: Sequelize.INTEGER
        },
        close_stock_sku: {
            type: Sequelize.INTEGER
        },
        open_stock: {
            type: Sequelize.INTEGER
        },
        open_stock_sku: {
            type: Sequelize.INTEGER
        },
        waste: {
            type: Sequelize.INTEGER
        },
        purchase_price: {
            type: Sequelize.DECIMAL(10, 2)
        }
    },
    options: {
        timestamps: false,
        freezeTableName: true
    }
}

module.exports = productStockMoveSchema;