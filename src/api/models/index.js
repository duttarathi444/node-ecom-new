'use strict';
// local imports
const { createConnections, SequelizeOp } = require("../datasources").MysqlDataSource;

const priceMasterSchema = require('./product/price_master');
const productCategoriesMasterSchema = require('./product/product_categories_master');
const productImageSchema = require('./product/product_image');
const productMasterSchema = require('./product/product_master');
const productPriceSchema = require('./product/product_price');
const productStockSchema = require('./product/product_stock');
const storeSchema = require('./product/store');
const productStockMoveSchema = require('./product/product_stock_movement');

const cartHeaderSchema = require('./cart/cart_header');
const cartDetailsSchema = require('./cart/cart_details');

const salesOrderHeaderSchema = require('./order/sales_order_header');
const salesOrderDetailsSchema = require('./order/sales_order_details');

const addressSchema = require('./address/address');

const salesOrderAddressSchema = require('./order/sales_order_address');

const salesOrderHistory = require('./order/cancel_order_history');

const customerMasterSchema = require('./user/customer_master');
const misUserSchema = require('./employee/mis_user');
const misUserLoginSchema = require('./employee/mis_user_login');
const orderStatusMasterSchema = require('./order/order_status_master');
const loginSchema = require('./user/login');

const uomSchema = require('./product/uom');

let dbModels;
let dbRef;
/**
 * helper functions
 */
async function createDatabases() {
    console.log(`\nIndexModel.createDatabases triggered`);
    // create all Database Connections
    const dbs = await createConnections(['db1']);
    const { db1 } = dbs;

    const Login = db1.define('login', loginSchema.schema, loginSchema.options);
    // price_master model
    const PriceMaster = db1.define('price_master', priceMasterSchema.schema, priceMasterSchema.options);
    // product_categorie_master model
    const ProductCategoriesMaster = db1.define('product_categories_master', productCategoriesMasterSchema.schema, productCategoriesMasterSchema.options);
    // product_master model
    const ProductMaster = db1.define('product_master', productMasterSchema.schema, productMasterSchema.options);
    // product_image model
    const ProductImage = db1.define('product_image', productImageSchema.schema, productImageSchema.options);
    // product_price model
    const ProductPrice = db1.define('product_price', productPriceSchema.schema, productPriceSchema.options);
    // product_stock model
    const ProductStock = db1.define('product_stock', productStockSchema.schema, productStockSchema.options);
    // store model
    const Store = db1.define('store', storeSchema.schema, storeSchema.options);
    // create product stock movement model
    const ProductStockMove = db1.define('product_stock_movement', productStockMoveSchema.schema, productStockMoveSchema.options);

    //uom details
    const Uom = db1.define('uom', uomSchema.schema, uomSchema.options);

    // relation between product master and product price
    ProductMaster.hasOne(ProductPrice, { foreignKey: 'prod_id' });
    ProductMaster.hasOne(ProductImage, { as: 'image_details', foreignKey: 'prod_id' });
    ProductMaster.hasOne(ProductStock, { as: 'stock_details', foreignKey: 'prod_id' });

    // cart header
    const CartHeader = db1.define('cart_header', cartHeaderSchema.schema, cartHeaderSchema.options);
    // cart details
    const CartDetails = db1.define('cart_details', cartDetailsSchema.schema, cartDetailsSchema.options);
    // crete relation between cart header and cart details
    CartHeader.hasMany(CartDetails, { foreignKey: 'cart_id' });

    // create sale order header
    const OrderHeader = db1.define('sales_order_header', salesOrderHeaderSchema.schema, salesOrderHeaderSchema.options);
    // create sale order details
    const OrderDetails = db1.define('sales_order_details', salesOrderDetailsSchema.schema, salesOrderDetailsSchema.options);
    // relation between order header and order details
    OrderHeader.hasMany(OrderDetails, { foreignKey: 'sales_order_id' });
    // address model
    const Address = db1.define('address', addressSchema.schema, addressSchema.options);
    // order sale history model
    const OrderHistory = db1.define('sales_order_history', salesOrderHistory.schema, salesOrderHistory.options);
    // create sale order address model
    const OrderAddress = db1.define('sales_order_address', salesOrderAddressSchema.schema, salesOrderAddressSchema.options);


    const CustomerMaster = db1.define('customer_master', customerMasterSchema.schema, customerMasterSchema.options);

    // mis_user model
    const MisUser = db1.define('mis_user', misUserSchema.schema, misUserSchema.options);

    // mis_user_login model
    const MisUserLogin = db1.define('mis_user_login', misUserLoginSchema.schema, misUserLoginSchema.options);

    const OrderStatusMaster = db1.define('order_status_master', orderStatusMasterSchema.schema, orderStatusMasterSchema.options);
    CustomerMaster.hasMany(Address, { as: 'address', foreignKey: 'customer_id' });

    dbModels = {
        db1Conn: db1,
        PriceMaster,
        ProductCategoriesMaster,
        ProductMaster,
        ProductImage,
        ProductPrice,
        ProductStock,
        Store,
        ProductStockMove,
        CartHeader,
        CartDetails,
        OrderHeader,
        OrderDetails,
        Address,
        OrderHistory,
        OrderAddress,
        CustomerMaster,
        MisUser,
        MisUserLogin,
        Login,
        OrderStatusMaster,
        Uom
    };

    return dbModels;
}

/**
 * Exports
 */
exports.getModels = () => {
    // console.log(`\nIndexModel.getModels triggered`);
    if (dbModels) {
        // console.log(`\ndbModels exist. Returning...`);
        return Promise.resolve(dbModels);
    } else {
        return createDatabases();
    }
}

exports.SequelizeOp = SequelizeOp;