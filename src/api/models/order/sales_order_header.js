const Sequelize = require("sequelize");

const SalesOrderHeaderSchema = {
    schema: {
        // attributes
        sales_order_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        trans_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV1
        },
        customer_id: {
            type: Sequelize.INTEGER
        },
        order_date: {
            type: Sequelize.STRING
        },
        status_id: {
            type: Sequelize.INTEGER
        },
        item_count: {
            type: Sequelize.INTEGER
        },
        total_price: {
            type: Sequelize.DECIMAL
        },
        expected_delivery_time: {
            type: Sequelize.STRING,
            defaultValue: "None"
        },
        actual_delivery_time: {
            type: Sequelize.STRING,
            defaultValue: "None"
        },
        pincode: {
            type: Sequelize.STRING
        },
        store_id: {
            type: Sequelize.INTEGER /* 1 - Ironing,  2 - Wash and ironing, 3 - Steam ironing, 4- Dry cleaning */
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = SalesOrderHeaderSchema;
