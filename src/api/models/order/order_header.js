const Sequelize = require("sequelize");

const orderHeaderSchema = {
    schema: {
        // attributes
        virtual_order_id: {
            type: Sequelize.STRING
        },
        order_id: {
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        trans_id: {
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV1
        },
        user_id: {
            type: Sequelize.INTEGER
        },
        order_date: {
            type: Sequelize.STRING
        },
        status_id: {
            type: Sequelize.INTEGER
        },
        item_count: {
            type: Sequelize.INTEGER
        },
        total_price: {
            type: Sequelize.DECIMAL
        },
        address: {
            type: Sequelize.JSON
        },
        expected_delivery_time: {
            type: Sequelize.STRING,
            defaultValue: "None"
        },
        actual_delivery_time: {
            type: Sequelize.STRING,
            defaultValue: "None"
        },
        pincode: {
            type: Sequelize.STRING
        },
        payment_mode: {
            type: Sequelize.STRING /* COD (Default) | ONLINE  */
        },
        payment_status: {
            type: Sequelize.STRING   /*(NA-for COD)| UNPAID (Default- my opinion) | PENDING | PAID | CANCLED  */
        },
        order_flag: {
            type: Sequelize.INTEGER /* 0 - TEMPORARY,  1 - PERMANENT, 2 - CANCELLED */
        },
        category_id: {
            type: Sequelize.INTEGER /* 1 - Ironing,  2 - Wash and ironing, 3 - Steam ironing, 4- Dry cleaning */
        }
    },
    options: {
        // options
        timestamps: false,
        freezeTableName: true,
    }
}

module.exports = orderHeaderSchema;
