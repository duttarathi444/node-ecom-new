const moment = require('moment');
const HttpStatus = require('http-status-codes');
const axios = require('axios');
// local imports
const env = require('../../../env');
const { getModels } = require('../../models');
const { pushToFirebase, dpushToFirebase } = require('../../lib/fcm_helper');
let orderHeader;
let address;
const utcOffsetMins = env.env.utcOffsetMins;
const order_creation_id = env.service_flow.order_creation_id;

exports.createOrder = async (req, res) => {
    const { OrderNotifications, Login, EmployeeLogin, UserMaster, DelboyOrderMapping, Employee, OrderHeader, OrderDetails, OrderHistory, Address, LaundryServiceFlow, CartDetails, CartHeader, ServiceAvailabilityPincode, db1Conn } = await getModels();
    // const dbRef = get_sequelize();
    console.log('\ncart.controller.createOrder  triggered -->');

    /*
    
    {
    "cart_id":1,
    "user_id":1,
    "order_date":"2019-12-16 05:39:00",
    "order_details":[
    
    {
    "prod_id":2,
    "quantity":2,
    "per_unit_price":5,
    "total_price":10
    },
      {
    "prod_id":3,
    "quantity":3,
    "per_unit_price":8,
    "total_price":24
    }
    ],
    "grand_total_quantity":5,
    "grand_total_price":34,
    "address":{
    "address1":"Lane-1",
    "address2":"Block-1",
    "address3":"Street-1",
    "city":"Cuttack",
    "state":"Odisha",
    "pincode":"753001",
    "longitude":"111111111.00001",
    "latitude":"2222222222.00002",
    "flag_default":"primary",
    "phoneno":"1234567890",
    }
    }
     */

    const t = await db1Conn.transaction();

    try {

        let serviceAvailable = await ServiceAvailabilityPincode.findOne({ where: { pincode: req.body.address.pincode } }, { transaction: t });

        if (serviceAvailable) {

            const holi_moment = moment().utc().utcOffset(utcOffsetMins);
            const holi_date_string = holi_moment.clone().format("DD/MM/YYYY");
            console.log("\n\nHOLI DATE STRING======", holi_date_string);
            if (holi_date_string !== '09/03/2020' || holi_date_string !== '10/03/2020') {
                console.log("\n\nNOT WITHIN HOLI DATE======");
                // Here status_id and module_id are hard coded, it will be dynamic in future
                let laundry_service_flow = await LaundryServiceFlow.findOne({ where: { status_id: order_creation_id, module_id: 1 } }, { transaction: t });
                console.log("\n\n LAUNDRY SERVICE FLOW======", JSON.stringify(laundry_service_flow, 0, 2));
                let cartHeader = await CartHeader.findOne({ where: { cart_id: req.body.cart_id, user_id: req.body.user_id } });
                if (cartHeader) {
                    if (req.body.grand_total_quantity >= 10) {

                        var format = 'hh:mm:ss a'
                        const startMoment_1 = moment('12:00:00 am', format).utc().utcOffset(utcOffsetMins);
                        const endMoment_1 = moment('08:00:00 am', format).utc().utcOffset(utcOffsetMins);

                        console.log("\n\n startMoment_1===", startMoment_1);
                        console.log("\n\n endMoment_1===", endMoment_1);
                        const startMoment_2 = moment('08:00:00 am', format).utc().utcOffset(utcOffsetMins);
                        const endMoment_2 = moment('12:00:00 pm', format).utc().utcOffset(utcOffsetMins);

                        const startMoment_3 = moment('12:00:00 pm', format).utc().utcOffset(utcOffsetMins);;
                        const endMoment_3 = moment('18:30:00 pm ', format).utc().utcOffset(utcOffsetMins);;

                        const startMoment_4 = moment('18:30:00 pm', format).utc().utcOffset(utcOffsetMins);;
                        const endMoment_4 = moment('23:59:59 pm ', format).utc().utcOffset(utcOffsetMins);;


                        let delivery_order_date_time = "";
                        const order_moment = moment().utc().utcOffset(utcOffsetMins);
                        const cancel_order_moment = order_moment.clone().add(15, 'minutes');

                        if (order_moment.isSameOrAfter(startMoment_1) && order_moment.isBefore(endMoment_1)) {
                            const delivery_date_string = order_moment.clone().format("DD/MM/YYYY");
                            const delivery_time_string = '10:00 am';
                            delivery_order_date_time = delivery_date_string + " " + delivery_time_string;
                            console.log("\n\n BLOCK ======11111", delivery_order_date_time);
                        }
                        else if (order_moment.isSameOrAfter(startMoment_2) && order_moment.isBefore(endMoment_2)) {
                            delivery_order_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
                            console.log("\n\n BLOCK ======2222", delivery_order_date_time);
                        }
                        else if (order_moment.isSameOrAfter(startMoment_3) && order_moment.isBefore(endMoment_3)) {
                            delivery_order_date_time = order_moment.clone().add(90, 'minutes').format("DD/MM/YYYY hh:mm a");
                            console.log("\n\n BLOCK ======33333", delivery_order_date_time);
                        }
                        else if (order_moment.isSameOrAfter(startMoment_4) && order_moment.isBefore(endMoment_4)) {
                            const delivery_date_string = order_moment.clone().add(1, 'day').format("DD/MM/YYYY");
                            console.log("\n\n delivery_date_string ======333", delivery_date_string);
                            const delivery_time_string = '10:00 am';
                            delivery_order_date_time = delivery_date_string + " " + delivery_time_string;
                            console.log("\n\n BLOCK ======4444", delivery_order_date_time);
                        }
                        var order_date_time = order_moment.format("DD/MM/YYYY hh:mm a");
                        let order_date_time_array = order_date_time.split(" ");
                        let order_date = order_date_time_array[0];
                        let order_time = order_date_time_array[1] + " " + order_date_time_array[2];

                        var cancel_date_time = cancel_order_moment.format("DD/MM/YYYY hh:mm a");
                        let cancel_date_time_array = cancel_date_time.split(" ");
                        let cancel_date = cancel_date_time_array[0];
                        let cancel_time = cancel_date_time_array[1] + " " + cancel_date_time_array[2];

                        console.log("\n\n order_date_time ======", order_date_time);
                        console.log("\n\n order_date  ======", order_date);

                        console.log("\n\n order_time  ======", order_time);


                        console.log("\n\n cancel_date_time ======", cancel_date_time);
                        console.log("\n\n cancel_date  ======", cancel_date);

                        console.log("\n\n cancel_time  ======", cancel_time);


                        console.log("\n\n ORDER MONENT ======", order_moment);
                        console.log("\n\nCANCEL  ORDER MONENT ======", cancel_order_moment);
                        console.log("\n\n DELIVERY ORDER MONENT ======", delivery_order_date_time);

                        // let statusDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm:ss a");
                        // let date_time_array = statusDate.split(" ");
                        // let date = date_time_array[0];
                        // let time = date_time_array[1] + " " + date_time_array[2];

                        orderHeader = await OrderHeader.create({
                            user_id: req.body.user_id, trans_id: req.body.trans_id, order_date: order_date_time,
                            status_id: laundry_service_flow.status_id, item_count: req.body.grand_total_quantity,
                            total_price: req.body.grand_total_price, address: req.body.address, expected_delivery_time: " ",
                            actual_delivery_time: " ",
                            pincode: req.body.address.pincode
                        },
                            { transaction: t });
                        console.log("\n\n ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));

                        if (req.body.address.address_id) {
                            let address = await Address.findOne({ where: { address_id: req.body.address.address_id }, attributes: ['address1', 'address2', 'address3', 'city', 'state', 'pincode', 'longitude', 'latitude', 'flag_default', 'phoneno'] });

                            console.log("\n\n EXISTING ADDRESS ======", JSON.stringify(address, 0, 2));
                            // orderHeader = await OrderHeader.create(
                            //     {
                            //         user_id: req.body.user_id, order_date: order_date_time,
                            //         status_id: laundry_service_flow.status_id, item_count: req.body.grand_total_quantity,
                            //         total_price: req.body.grand_total_price, address: address, expected_delivery_time: " ", actual_delivery_time: " ",
                            //         pincode: req.body.address.pincode
                            //     },
                            //     { transaction: t });

                            await Address.update(
                                { flag_default: 'secondary' },
                                { where: { user_id: req.body.user_id } }, { transaction: t });

                            await Address.update(
                                { flag_default: req.body.address.flag_default },
                                { where: { address_id: req.body.address.address_id } }, { transaction: t });

                        }
                        else {

                            await Address.update(
                                { flag_default: 'secondary' },
                                { where: { user_id: req.body.user_id } }, { transaction: t });

                            const address = await Address.create(
                                {
                                    address1: req.body.address.address1, address2: req.body.address.address2, address3: req.body.address.address3, city: req.body.address.city, state: req.body.address.state,
                                    pincode: req.body.address.pincode, longitude: req.body.address.longitude, latitude: req.body.address.latitude, flag_default: req.body.address.flag_default, user_id: req.body.user_id, phoneno: req.body.address.phoneno,
                                },
                                { transaction: t });
                            console.log("\n\n ADDRESS CREATED======", JSON.stringify(address, 0, 2));

                        }

                        await Promise.all(req.body.order_details.map(async (element) => {
                            console.log("\n\n ORDER_DETAIL======", JSON.stringify(element, 0, 2));
                            let orderDetails = await OrderDetails.create(
                                { order_id: orderHeader.order_id, user_id: orderHeader.user_id, prod_id: element.prod_id, customer_order_quantity: element.quantity, dboy_order_accept_quantity: 0, lman_order_accept_quantity: 0 },
                                { transaction: t });
                            console.log("\n\n ORDER_DETAIL ADDED SUCCESSFULLY ")
                        }));


                        console.log("\n\n ORDERHEADER CREATED ======", JSON.stringify(orderHeader, 0, 2));
                        console.log("\n\n ORDERHEADER ITEM COUNT ======", orderHeader.item_count);
                        console.log("\n\n ORDERHEADER ITEM PRICE ======", orderHeader.total_price);

                        console.log("\n\n ORDER DATE ======", order_date);
                        console.log("\n\n ORDER TIME ======", order_time);



                        const orderHistory = await OrderHistory.create(
                            {
                                order_id: orderHeader.order_id, status_id: laundry_service_flow.status_id, status_date: order_date, active_flag: laundry_service_flow.status_desc, user_id: req.body.user_id, status_time: order_time, item_count: orderHeader.item_count,
                                total_price: orderHeader.total_price
                            },
                            { transaction: t });
                        console.log("\n\n ORDER HISTORY CREATED======", JSON.stringify(orderHistory, 0, 2));


                        let deleteCart = await CartHeader.destroy({ where: { user_id: req.body.user_id, cart_id: req.body.cart_id } }, { transaction: t });

                        let cartDetails = await CartDetails.findAll({ where: { cart_id: req.body.cart_id } }, { transaction: t });
                        await Promise.all(cartDetails.map(async (element) => {
                            await CartDetails.destroy({ where: { cart_id: element.cart_id } }, { transaction: t });
                        }));

                        t.commit();

                        // let url = env.sms.URI + phoneno + '&text=OTP for IESTRE is ' + otp + '. Do not share with anyone.&priority=ndnd&stype=normal';
                        // let resopnse = await axios.get(url);

                        let x = 0;
                        var refreshId = setInterval(function () {
                            x = x + 1;
                            console.log("\n\n TRY TO SEND MESSAGE FOR TIME :" + x);
                            var result = sendSMS(orderHeader.order_id, orderHeader.user_id, orderHeader.address);
                            if (result) {
                                console.log("\n\n MESSAGE SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                                clearInterval(refreshId);

                            }
                            if (x === 3) {
                                console.log("\n\nTRY FOR TRHEE TIMES, MESSAGE NOT SEND  SUCCESSFULLY TO DELBOY AND CUSTOMER");
                                clearInterval(refreshId);
                            }

                        }, 5000);



                        try {
                            let empname = "";
                            let emp_phoneno = "";
                            let msg_body1 = "";
                            let login = await Login.findOne({ where: { user_id: orderHeader.user_id } });
                            let user = await UserMaster.findOne({ where: { user_id: orderHeader.user_id } });
                            let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: orderHeader.order_id, delivery_type: 1 } });
                            if (dboy_order_mapping) {
                                let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                                empname = employee.emp_fname + " " + employee.emp_lname;
                                emp_phoneno = employee.emp_contact_no;
                            }


                            let employee_login = await EmployeeLogin.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                            const msg_body = 'Thanks for ordering. Order no ' + orderHeader.order_id +
                                ' will be pick up by our delivery boy ' + empname + ', phone no-' + emp_phoneno;

                            let customername = user.user_fname + " " + user.user_lname;
                            let customerphoneno = "";
                            if (user.user_phoneno) {
                                console.log("\nDELIVERYBOY NOTIFICATION USER PHONE NO :" + user.user_phoneno);
                                msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                                customerphoneno = user.user_phoneno;
                            }
                            else {
                                console.log("\nDELIVERYBOY NOTIFIACTION USER HAVING NO PHONE NO :");
                                msg_body1 = 'Customer name-' + customername + ', order no ' + orderHeader.order_id + ', address-http://maps.google.com/?q=' + orderHeader.address.latitude + ',' + orderHeader.address.longitude;
                                customerphoneno = "";
                            }


                            const dorderNotifications = await OrderNotifications.create(
                                {
                                    order_id: orderHeader.order_id,
                                    status_id: laundry_service_flow.status_id,
                                    emp_id: dboy_order_mapping.emp_id,
                                    role_id: 2,
                                    msg_title: 'Order has to be receive',
                                    msg_body: msg_body1,
                                    created_at: order_date_time,
                                    latitude: orderHeader.address.latitude,
                                    longitude: orderHeader.address.longitude
                                });


                            const orderNotifications = await OrderNotifications.create(
                                {
                                    order_id: orderHeader.order_id,
                                    status_id: laundry_service_flow.status_id,
                                    user_id: req.body.user_id,
                                    msg_title: 'Order Created Successfully',
                                    msg_body: msg_body,
                                    created_at: order_date_time
                                });


                            const fetchBody = {
                                notification: {
                                    title: "iestre",
                                    text: msg_body,
                                    click_action: "com.purpuligo.laundry.NotificationActivity"
                                },
                                data: {
                                    deliveryBoyName: empname,
                                    orderId: orderHeader.order_id,
                                    delBoyPhoneNo: emp_phoneno
                                },
                                to: login.fcmToken
                            };

                            const fetchBody1 = {
                                notification: {
                                    title: "iestre",
                                    text: msg_body1,
                                    click_action: "com.purpuligo.laundry.NotificationActivity"
                                },
                                data: {
                                    customerName: customername,
                                    orderId: orderHeader.order_id,
                                    customerPhoneNo: customerphoneno
                                },
                                to: employee_login.fcmToken
                            };
                            let result1 = dpushToFirebase(fetchBody1, 3);
                            console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");

                            let y = 0;
                            var notificationId = setInterval(function () {
                                y = y + 1;
                                console.log("\n\n TRY TO SEND NOTIFICATION TO FIREBASE :" + y);
                                let result = pushToFirebase(fetchBody, 3);
                                if (result) {
                                    console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO CUSTOMER");
                                    //dpushToFirebase(fetchBody1, 3);
                                    // console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY");
                                    clearInterval(notificationId);
                                }
                                // if (y === 1) {
                                //     console.log("\n\nNOTIFICATION NOT SEND  SUCCESSFULLY TO DELBOY CUSTOMER");
                                //     clearInterval(notificationId);
                                // }

                            }, 120000);

                        }
                        catch (error) {
                            console.log("\n\n NOTIFICATION ERROR ", error);
                        }

                        console.log("\n\n ORDER ADDED SUCCESSFULLY ");
                        orderHeader.setDataValue('cancel_date_time', cancel_date_time);
                        orderHeader.setDataValue('order_receive_date_time', delivery_order_date_time);
                        res.json({ status: 1, message: "order added successfully", data: orderHeader })

                    }
                    else {
                        res.json({ status: 0, message: "Order can't be placed .Item order qauntity should not less than 10", data: {} })
                    }
                }
                else {
                    console.log('\n\n Given user_id and cart_id does not exist in cart_header table, Sending Error Response...');
                    res.json({ status: 0, message: "Given user_id and cart_id does not exist in cart_header table", data: {} })
                }

            }
            else {
                console.log("\n\nWITHIN HOLI DATE======");
                console.log("\n\nORDER CAN'T BE PLACE======");
                res.json({ status: 0, message: "Dear Customer, Thanks for choosing our service. Please note due to Holi festival, we will not be able to service you on 9/03/2020 and 10/03/202. Wish you & your family a very happy and safe Holi!!", data: {} })
            }
        }
        else {
            res.json({ status: 0, message: "service not available", data: {} })
        }
    }
    catch (error) {
        t.rollback()
        console.log(error);
        res.status(404).send(error);
    }

}





exports.cancelOrder = async (req, res) => {
    console.log('\norder.controller.cancelOrder  triggered -->');
    const { OrderNotifications, UserMaster, EmployeeLogin, OrderHeader, OrderDetails, OrderHistory, CancelOrderHistory, DelboyOrderMapping, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();

    // OrderHeader.hasMany(OrderDetails, { as: 'order_details', foreignKey: 'order_id' }); //puts foreignKey order_id in order_details table
    // OrderHeader.hasMany(OrderHistory, { as: 'order_history', foreignKey: 'order_id' });
    try {
        const order = await OrderHeader.findOne({
            where: { user_id: req.body.user_id, order_id: req.body.order_id },
            include: [
                { model: OrderDetails, as: 'order_details' },
                { model: OrderHistory, as: 'order_history' }
            ],
            order: [['order_id', 'DESC']]
        });
        const order_date_time = order.order_date;
        var format = 'DD/MM/YYYY hh:mm a';
        // const db_order_moment = moment(order_date_time, format).utc().utcOffset(utcOffsetMins);
        // const cancel_order_moment = db_order_moment.clone().add(15, 'minutes');
        //const db_order_moment = moment.utc(order_date_time, format, true).utcOffset(utcOffsetMins);

        const db_order_moment = moment.utc(order_date_time, format, true)
            .utcOffset(utcOffsetMins)
            .subtract(utcOffsetMins, 'minutes');

        const cancel_order_moment = db_order_moment.clone().add(15, 'minutes');

        // const cancel_moment = moment().utc().utcOffset(utcOffsetMins);
        const current_cancel_moment = moment().utc().utcOffset(utcOffsetMins);
        if (current_cancel_moment.isSameOrBefore(cancel_order_moment)) {
            //console.log("\n\n HELLOOOOOOOOO======db_order_moment", db_order_moment);
            //console.log("\n\n HELLOOOOOOOOO======cancel_order_moment", cancel_order_moment);
            //console.log("\n\n HELLOOOOOOOOO======current_cancel_moment", current_cancel_moment);

            //console.log("\n\n ORDER LIST======", JSON.stringify(order, 0, 2));
            const t = await db1Conn.transaction();
            await OrderHeader.destroy({ where: { user_id: req.body.user_id, order_id: req.body.order_id } }, { transaction: t });
            let orderDetails = await OrderDetails.findAll({ where: { user_id: req.body.user_id, order_id: req.body.order_id } }, { transaction: t });
            await Promise.all(orderDetails.map(async (element) => {
                await OrderDetails.destroy({ where: { user_id: element.user_id, order_id: element.order_id } }, { transaction: t });
            }));

            await OrderHistory.destroy({ where: { order_id: req.body.order_id } }, { transaction: t });
            let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: req.body.order_id, delivery_type: 1 } }, { transaction: t });
            await DelboyOrderMapping.destroy({ where: { order_id: req.body.order_id } }, { transaction: t });

            //let cancelOrderDate = moment().utcOffset("+05:30").format("YYYY-MM-DD HH:mm:ss");
            let cancelOrderDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm:ss a");
            await CancelOrderHistory.create(
                {
                    cancel_user_id: order.user_id, cancel_order_id: order.order_id, cancel_order_details: order,
                    cancel_order_date: cancelOrderDate
                }, { transaction: t });
            t.commit()
            console.log("\n\n ORDER CANCELEDED SUCCESSFULLY ");


            try {
                let userDetails = await UserMaster.findOne({ where: { user_id: order.user_id } });
                let customer_name = "";
                let delby_fcmToken = "";
                let msg_body_forDBoy = "";
                if (userDetails.login_type === 'Otp') {
                    customer_name = userDetails.user_fname
                }
                else {
                    customer_name = userDetails.user_fname + " " + userDetails.user_lname;
                }

                if (dboy_order_mapping) {
                    let employee_login = await EmployeeLogin.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                    delby_fcmToken = employee_login.fcmToken;

                    msg_body_forDBoy = 'Order No -' + order.order_id +
                        ' of customer  name -' + customer_name + ' has been canceled.';
                }


                const fetchBody = {
                    notification: {
                        title: "iestre",
                        text: msg_body_forDBoy,
                        click_action: "com.maks.iestredeliveryboy.NotificationActivity"
                    },
                    data: {
                        orderId: order.order_id
                    },
                    to: delby_fcmToken
                };
                let result = dpushToFirebase(fetchBody, 3);
                console.log("\n\n NOTIFICATION SEND  SUCCESSFULLY TO DELIVERY BOY FOR ORDER PICKUP AFTER ORDER COMPLETE BY LMAN");

                const dorderNotifications = await OrderNotifications.create(
                    {
                        order_id: order.order_id,
                        status_id: 1060,
                        user_id: order.user_id,
                        emp_id: dboy_order_mapping.emp_id,
                        role_id: 2,
                        msg_title: 'Order has been canceled',
                        msg_body: msg_body_forDBoy,
                        created_at: cancelOrderDate
                    });


                console.log("\n\n CANCEL NOTIFICATION ENTERY  SUCCESSFULLY ");
            }
            catch (error) {
                console.log("\n\n NOTIFICATION NOT SEND  SUCCESSFULLY TO DELIVERY BOY FOR CANCEL ORDER ERROR " + error);
            }



            res.json({ status: 1, message: "order canceled successfully", data: order })
        }

        else {
            console.log("\n\n HIIIIIIIIIIIIIIIIIIIIIIIIII");
            console.log('\n\n Order cancel time exceed, Sending Error Response...');
            // const errorResponse = {
            //     status: 0,
            //     message: 'Order cancel time exceed',
            // };
            // return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
            res.json({ status: 0, message: "Order cancel time exceed", data: {} })
        }
        // console.log("\n\n ORDER LIST======", JSON.stringify(order, 0, 2));
        // const t = await db1Conn.transaction();
        // await OrderHeader.destroy({ where: { user_id: req.body.user_id, order_id: req.body.order_id } }, { transaction: t });
        // let orderDetails = await OrderDetails.findAll({ where: { user_id: req.body.user_id, order_id: req.body.order_id } }, { transaction: t });
        // await Promise.all(orderDetails.map(async (element) => {
        //     await OrderDetails.destroy({ where: { user_id: element.user_id, order_id: element.order_id } }, { transaction: t });
        // }));

        // await OrderHistory.destroy({ where: { order_id: req.body.order_id } }, { transaction: t });

        // //let cancelOrderDate = moment().utcOffset("+05:30").format("YYYY-MM-DD HH:mm:ss");
        // let cancelOrderDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm:ss a");
        // await CancelOrderHistory.create(
        //     {
        //         cancel_user_id: order.user_id, cancel_order_id: order.order_id, cancel_order_details: order,
        //         cancel_order_date: cancelOrderDate
        //     }, { transaction: t });
        // t.commit()
        // console.log("\n\n ORDER CANCELEDED SUCCESSFULLY ")
        // res.json({ status: 1, message: "order canceled successfully", data: order })
    }
    catch (error) {
        console.log("ERROR==" + error);
        // res.status(404).send(error);
        //res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
        throw error;
    }
}


exports.listOrderDetails = async (req, res) => {
    console.log('\norder.controller.listOrderDetails  triggered -->');
    const { OrderHeader, OrderDetails, OrderHistory, ProductMaster, PriceMaster } = await getModels();
    try {
        const orders = await OrderHeader.findAll({
            where: { user_id: req.params.user_id },
            include: [
                { model: OrderDetails, as: 'order_details' },
                { model: OrderHistory, as: 'order_history' }
            ],
            order: [['order_id', 'DESC']]
        });

        console.log("\n\n ORDER LIST======", JSON.stringify(orders, 0, 2));


        await Promise.all(orders.map(async (element) => {
            console.log("\n\n ORDER Details======", JSON.stringify(element.order_details, 0, 2));

            await Promise.all(element.order_details.map(async (element_1) => {
                console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

                // here select price from price_master based on prod_id
                let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                console.log('\nproduct ' + element_1.prod_id + ' price of ' + product.prod_price);



                // here select product_name from product_master based on prod_id
                let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));


                // let quantity = parseInt(element.quantity);
                // console.log('\nProduct quantity ' + quantity);
                // let total_price = parseFloat(product.prod_price) * quantity;

                // console.log('\nTotal Price ' + total_price);
                element_1.setDataValue('prod_name', product_master.prod_name);
                element_1.setDataValue('prod_code', product_master.prod_code);
                element_1.setDataValue('per_unit_price', product.prod_price);
                // element.setDataValue('total_price', total_price.toFixed(2));

            }));

        }));


        res.json({ status: 1, message: "list of orders ", data: orders })
    }
    catch (error) {
        console.log("ERROR==" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}


exports.completedOrderList = async (req, res) => {
    try {
        console.log('\norder.controller.completedOrderList  triggered -->');
        const { OrderHeader, OrderDetails, OrderHistory, ProductMaster, Address, LaundryServiceFlow, CartDetails, CartHeader, db1Conn } = await getModels();
        //let attributes = ['OrderHeader.order_id', 'OrderHeader.user_id', 'OrderHeader.order_date', 'OrderHeader.item_count_upd', 'OrderHeader.expected_delivery_time'];
        let attributes = ['order_id', 'trans_id', 'user_id', 'order_date', 'status_id', 'item_count', 'total_price', 'expected_delivery_time', 'actual_delivery_time'];
        let odr_ids = [];
        let orderHistories;
        orderHistories = await OrderHistory.findAll({
            where: { user_id: req.body.user_id, status_id: 1050 },
            order: [['status_date', 'DESC'],
            ['status_time', 'DESC']]
        });

        let orders = [];
        console.log("\n\nCOMPLETE ORDERS FROM ORDER HISTORIES======", JSON.stringify(orderHistories, 0, 2));
        if (orderHistories) {

            await Promise.all(orderHistories.map(async (element) => {
                //         console.log('\nUser--' + element.user_id + ' having  orderId ' + element.order_id);
                //         // here select all order deatils list  from order_details  based on user_id and order_id
                //         let orderDetails = await OrderDetails.findAll({ where: { user_id: element.user_id, order_id: element.order_id } });
                console.log("\n\nORDER_ID======" + element.order_id);

                const order = await OrderHeader.findOne({
                    where: { order_id: element.order_id }, attributes: attributes,
                    include: [
                        { model: OrderDetails, as: 'order_details' },
                        { model: OrderHistory, where: { status_id: 1050 }, as: 'order_history', attributes: ['status_date', 'status_time'] }
                    ]
                });
                orders.push(order);

            }));

            await Promise.all(orders.map(async (element) => {
                console.log("\n\n COMPLETE ORDER Details======", JSON.stringify(element.order_details, 0, 2));

                await Promise.all(element.order_details.map(async (element_1) => {
                    console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

                    // here select price from price_master based on prod_id
                    // let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
                    // console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
                    // console.log('\nproduct ' + element_1.prod_id + ' price of ' + product.prod_price);



                    // here select product_name from product_master based on prod_id
                    let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
                    console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));


                    // let quantity = parseInt(element.quantity);
                    // console.log('\nProduct quantity ' + quantity);
                    // let total_price = parseFloat(product.prod_price) * quantity;

                    // console.log('\nTotal Price ' + total_price);
                    element_1.setDataValue('prod_name', product_master.prod_name);
                    element_1.setDataValue('prod_code', product_master.prod_code);
                    element_1.setDataValue('per_unit_price', product_master.prod_price);
                    // element.setDataValue('total_price', total_price.toFixed(2));

                }));

            }));

            console.log("\n\nCOMPLETE ORDER LIST DETAILS======", JSON.stringify(orders, 0, 2));
            res.json({ status: 1, message: "complete order details", data: orders });

        }
        else {
            // res.json({ status: 1, message: "cart details", data: "no cart" });
            res.json({ status: 0, message: "empty order details list", data: {} });
        }
    }
    catch (error) {
        console.log(error);
        res.status(404).send(error);
    }
}


function sendSMSDemo() {
    console.log("hello world..... ");
}



sendSMS = async (order_id, user_id, address) => {
    console.log('\norder.controller.sendSMS triggered -->');
    const { UserMaster, DelboyOrderMapping, Employee, db1Conn } = await getModels();
    console.log("\nORDER_ID :" + order_id);
    console.log("\nORDER_ID :" + user_id);
    console.log("\n\nADDRESS :", JSON.stringify(address, 0, 2));
    /*  {
        user_id: req.user_id
    }
    */
    try {
        let user = await UserMaster.findOne({ where: { user_id: user_id } });
        let dboy_order_mapping = await DelboyOrderMapping.findOne({ where: { order_id: order_id, delivery_type: 1 } });
        if (user) {
            let customername = user.user_fname + " " + user.user_lname;
            let url1;
            let url;

            if (dboy_order_mapping) {
                let employee = await Employee.findOne({ where: { emp_id: dboy_order_mapping.emp_id } });
                let empname = employee.emp_fname + " " + employee.emp_lname;
                let emp_phoneno = employee.emp_contact_no;
                if (user.user_phoneno) {
                    console.log("\nUSER PHONE NO :" + user.user_phoneno);
                    url = env.sms.URI + emp_phoneno + '&text=Customer name-' + customername + ', order no ' + order_id + ', phone no-' + user.user_phoneno + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                }
                else {
                    console.log("\nUSER HAVING NO PHONE NO :");
                    url = env.sms.URI + emp_phoneno + '&text=Customer name-' + customername + ', order no ' + order_id + ', address-http://maps.google.com/?q=' + address.latitude + ',' + address.longitude + '&priority=ndnd&stype=normal';
                }
                let resopnse = await axios.get(url);

                if (resopnse.data) {
                    console.log("\nresopnse.data :" + resopnse.data);
                    if (user.user_phoneno) {
                        url1 = env.sms.URI + user.user_phoneno + '&text=Thanks for ordering. Order no ' + order_id + ' will be pick up by our delivery boy ' + empname + ', phone no-' + emp_phoneno + '&priority=ndnd&stype=normal';
                        let resopnse1 = await axios.get(url1);
                        console.log("\nresopnse1.data :" + resopnse1.data);
                    }
                    console.log("\nRETURN  :TRUE");
                    return true;
                }
                else {
                    console.log("\nRETURN  :FALSE");
                    return false;
                }
            }
            else {
                console.log("\nRETURN OUTER IF :FALSE");
                return false;
            }
        }
    }
    catch (error) {
        console.log('\norder.controller.sendSMS  error', error);
        throw error;

    }
}





// exports.ordersDetailReport = async (req, res) => {
//     console.log('\norder.controller.ordersDetailReport triggered -->');
//     const { OrderHeader, OrderDetails, OrderHistory, Employee, PriceMaster, ProductMaster, db1Conn } = await getModels();
//     try {
//         const { fromDate, toDate } = req.body;
//         console.log("\nREQUESTED FROM  DATE:" + fromDate);
//         console.log("\nREQUESTED FROM  DATE:" + toDate);
//         const orders = await OrderHeader.findAll({
//             where: {
//                 created_at: {
//                     "$between": [fromDate, toDate]
//                 }
//             },
//             include: [
//                 { model: OrderDetails, as: 'order_details' },
//                 { model: OrderHistory, as: 'order_history' }
//             ],
//             order: [['order_id', 'DESC']]
//         });

//         console.log("\n\n ORDER LIST======", JSON.stringify(orders, 0, 2));


//         await Promise.all(orders.map(async (element) => {
//             console.log("\n\n ORDER Details======", JSON.stringify(element.order_details, 0, 2));

//             // Select customer_name from user_master based on user_id found in order_header
//             let userDetails = await UserMaster.findOne({ where: { user_id: element.user_id } });
//             console.log("\n\nUSER MASTER DETAILS======", JSON.stringify(userDetails, 0, 2));
//             let customer_name = "";
//             let customer_phoneno = "";
//             if (userDetails.login_type === 'Otp') {
//                 customer_name = userDetails.user_fname;
//                 customer_phoneno = userDetails.user_phoneno;
//             }
//             else {
//                 customer_name = userDetails.user_fname + " " + userDetails.user_lname;
//                 customer_phoneno = userDetails.user_phoneno;
//             }

//             await Promise.all(element.order_details.map(async (element_1) => {
//                 console.log("\n\n INV ORDER Details======", JSON.stringify(element_1, 0, 2));

//                 // here select price from price_master based on prod_id
//                 let product = await PriceMaster.findOne({ where: { prod_id: element_1.prod_id, prod_status: 'CurPrice' } });
//                 console.log("PRODUCT DETAILS======", JSON.stringify(product, 0, 2));
//                 console.log('\nproduct ' + element_1.prod_id + ' price of ' + product.prod_price);



//                 // here select product_name from product_master based on prod_id
//                 let product_master = await ProductMaster.findOne({ where: { prod_id: element_1.prod_id } });
//                 console.log("PRODUCT MASTER DETAILS======", JSON.stringify(product_master, 0, 2));


//                 // let quantity = parseInt(element.quantity);
//                 // console.log('\nProduct quantity ' + quantity);
//                 // let total_price = parseFloat(product.prod_price) * quantity;

//                 // console.log('\nTotal Price ' + total_price);
//                 element_1.setDataValue('prod_name', product_master.prod_name);
//                 element_1.setDataValue('prod_code', product_master.prod_code);
//                 element_1.setDataValue('per_unit_price', product.prod_price);
//                 // element.setDataValue('total_price', total_price.toFixed(2));

//             }));




//             //===========================================
//             // Select delivery boy id  from order_history  based on  order_id
//             let orderHistory = await OrderHistory.findAll({ where: { order_id: element.order_id, role_id: 2 } });
//             console.log("\n\nORDER HISTORY======", JSON.stringify(orderHistory, 0, 2));
//             await Promise.all(orderHistory.map(async (element_2) => {
//                 console.log("\n\n find DELIVERY BOY ORDER HISTORY Details======", JSON.stringify(element_2, 0, 2));

//                 // here select Delivery name and phone no based on emp_id found in order history
//                 let deliveryBoy = await Employee.findOne({ where: { emp_id: element_2.emp_id } });
//                 console.log("EMPLOYEE DETAILS======", JSON.stringify(deliveryBoy, 0, 2));
//                 let delboy_contactno = "";
//                 if (deliveryBoy.emp_contact_no2) {
//                     delboy_contactno = deliveryBoy.emp_contact_no + ' , ' + deliveryBoy.emp_contact_no2;
//                 }
//                 else {
//                     delboy_contactno = deliveryBoy.emp_contact_no;
//                 }
//                 console.log('\nORDER NO: ' + element.order_id + ' - DELIVERY BOY EMPLOYEE-ID ' + deliveryBoy.emp_id + '- NAME - ' + deliveryBoy.emp_fname + ' ' + deliveryBoy.emp_lname + ' PHONE NO -' + delboy_contactno);
//                 element.setDataValue('delivery_partner_name', deliveryBoy.emp_fname + ' ' + deliveryBoy.emp_lname);
//                 element.setDataValue('delivery_partner_phone_no', delboy_contactno);
//             }));
//             //=========================================

//             //===========================================
//             // Select laundry man  id  from order_history  based on  order_id
//             let lorderHistory = await OrderHistory.findAll({ where: { order_id: element.order_id, role_id: 1 } });
//             console.log("\n\nLAUNDRY MAN ORDER HISTORY======", JSON.stringify(lorderHistory, 0, 2));
//             await Promise.all(lorderHistory.map(async (element_3) => {
//                 console.log("\n\n find LAUNDRY MAN  ORDER HISTORY Details======", JSON.stringify(element_3, 0, 2));

//                 // here select Laundry man  name and phone no based on emp_id found in order history
//                 let laundryMan = await Employee.findOne({ where: { emp_id: element_3.emp_id } });
//                 console.log("LAUNDRY MAN EMPLOYEE DETAILS======", JSON.stringify(laundryMan, 0, 2));
//                 let lman_contactno = "";
//                 if (laundryMan.emp_contact_no2) {
//                     lman_contactno = laundryMan.emp_contact_no + ' , ' + laundryMan.emp_contact_no2;
//                 }
//                 else {
//                     lman_contactno = laundryMan.emp_contact_no;
//                 }
//                 console.log('\nORDER NO: ' + element.order_id + ' - LAUNDRY MAN EMPLOYEE-ID ' + laundryMan.emp_id + '- NAME - ' + laundryMan.emp_fname + ' ' + laundryMan.emp_lname + ' PHONE NO -' + lman_contactno);
//                 element.setDataValue('laundry_partner_name', laundryMan.emp_fname + ' ' + laundryMan.emp_lname);
//                 element.setDataValue('laundry_partner_phone_no', lman_contactno);
//             }));
//             //=========================================

//             element.setDataValue('customer_name', customer_name);
//             element.setDataValue('customer_phoneno', customer_phoneno);

//         }));


//         res.json({ status: 1, message: "orders details ", data: orders })
//     }
//     catch (error) {
//         console.log('\norder.controller.ordersDetailReport Error triggered -->');
//         console.log("\norder.controller.ordersDetailReport Error triggered -->" + error);
//         // res.status(404).send(error);
//         res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
//     }
// }
