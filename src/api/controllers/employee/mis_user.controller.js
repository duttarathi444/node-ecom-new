// global import
const HttpStatus = require('http-status-codes');
const jwt = require('jsonwebtoken');
const moment = require('moment');
const axios = require('axios');
const Sequelize = require("sequelize");
const op = Sequelize.Op;
// local imports
const { getModels } = require('../../models');
const { pushToFirebase, dpushToFirebase, lpushToFirebase } = require('../../lib/fcm_helper');
const env = require('../../../env');
const utcOffsetMins = env.env.utcOffsetMins;
const jwtSecretKey = '123##$$)(***&';
var sequelize = new Sequelize('laundry_db', 'nodeuser', 'nodeuser@1234', {
    host: 'localhost',
    dialect: 'mysql'
});

/*
const env = require('../../../env');
const deliveryBoyDeliverCompleteStatusId = env.service_flow.deliveryboy_deliver_to_customer_status_id;// 1050
const deliveryBoyAcceptOrderFromLmanStatusId = env.service_flow.deliveryboy_accept_from_laundryman_status_id;// 1040
    order_creation_id: 1001,
    laundry_man_accept_status_id: 1020,
    laundry_man_complete_status_id: 1030,
    deliveryboy_accept_from_customer_status_id: 1010,
    deliveryboy_accept_from_laundryman_status_id: 1040,
    deliveryboy_deliver_to_customer_status_id: 1050

*/

// MisUser Login
exports.misUserLogin = async (req, res) => {
    const { MisUserLogin, Store, db1Conn } = await getModels();
    console.log('\nmis_user.controller.misUserLogin  triggered -->');

    console.log(`req.body: `, req.body);
    // phoneno is same as otp here
    const { mis_user_login_id, mis_user_password, store_id, fcmToken } = req.body;
    //fcmToken = "fcmToken";
    console.log(`MisUser Login-Id:  `, mis_user_login_id);
    console.log(`MisUser Password:  `, mis_user_password);
    console.log(`MisUser Store-Id:  `, store_id);
    console.log(`MisUser Fcm Token:  `, fcmToken);
    try {
        // Check mis_user given login_id and password match in mis_user_login table or not
        let mis_user = await MisUserLogin.findOne({ where: { mis_user_login_id: mis_user_login_id, mis_user_password: mis_user_password, store_id: store_id } });

        if (mis_user) {

            console.log(`\n\nEmployee details existing in emplolee_login table...`);

            // create an access token
            const access_token = jwt.sign({ id: mis_user.mis_user_id }, jwtSecretKey, {
                expiresIn: '365d', // expires in X secs
            });
            let lastAccessDate = moment().utcOffset("+05:30").format("DD/MM/YYYY HH:mm A");
            await MisUserLogin.update(
                { access_token: access_token, mis_user_last_login_date_time: lastAccessDate, fcmToken: fcmToken },
                { where: { mis_user_id: mis_user.mis_user_id } }
            )

            let misUserLoginDetails = await MisUserLogin.findOne({ where: { mis_user_id: mis_user.mis_user_id } });

            const storeDetails = await Store.findOne({ where: { store_id: store_id } });

            misUserLoginDetails.setDataValue('store_name', storeDetails.store_name);

            res.json({ status: 1, message: "mis_user_login details with new access_token", data: misUserLoginDetails })


        }
        else {
            console.log('\n\n MisUser given login_id and password does not match, Sending Error Response...');
            // const errorResponse = {
            //     status: 0,
            //     message: 'login_id  and password not match. You are unauthenticated',
            //     data: {}
            // };
            // return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
            res.json({ status: 0, message: "login_id  and password not match. You are unauthenticated", data: {} });
        }

    } catch (error) {
        console.log(error);
        //res.status(404).send(error);
        // res.json({ status: 0, message: "auth error", data: JSON.stringify(error, 0, 2) });
        throw error
    }
}


//MisUser Logout 
exports.misUserLogout = async (req, res) => {
    console.log('\nmis_user.controller.logout triggered -->');
    const { MisUserLogin, db1Conn } = await getModels();

    /*  {
        mis_user_id: req.mis_user_id
    }
    */
    try {
        let misUser = await MisUserLogin.findOne({ where: { mis_user_id: req.body.mis_user_id } });
        if (!misUser) {
            const errorResponse = {
                status: 0,
                message: 'Invalid user',
                data: {}
            };
            return res.status(HttpStatus.BAD_REQUEST).send(errorResponse);
        }
        else {
            let deleteAccessToken = await MisUserLogin.update(
                { access_token: null },
                { where: { mis_user_id: req.body.mis_user_id } }
            );
            if (deleteAccessToken) {
                res.json({ status: 1, message: "'MisUser logout successful", data: {} });
            }
            else {
                res.json({ status: 0, message: "'MisUser already logout ", data: {} });
            }
        }

    }
    catch (error) {
        console.log('\nmis_usercontroller.logout error', error)
        throw error;

    }
}

exports.customerList = async (req, res) => {
    console.log('\nmis_user.controller.customerList triggered -->');
    const { CustomerMaster, Address, MisUserLogin, db1Conn } = await getModels();
    try {
        const { fromDate, toDate, mis_user_login_id } = req.body;
        console.log("\nREQUESTED FROM  DATE:" + fromDate);
        console.log("\nREQUESTED FROM  DATE:" + toDate);
        console.log("\nMIS USER LOGIN ID:" + mis_user_login_id);
        // const current_moment = moment().utc().utcOffset(utcOffsetMins);
        // let current_date_time = current_moment.format("DD/MM/YYYY hh:mm A");
        // let current_date_time_array = current_date_time.split(" ");
        // let current_date = current_date_time_array[0];
        // let current_time = current_date_time_array[1] + " " + current_date_time_array[2];
        let userDetails = [];
        let misUser = await MisUserLogin.findOne({ where: { mis_user_login_id: mis_user_login_id } });
        console.log(JSON.stringify(misUser));
        if (misUser) {
            let userDetails1;
            if (misUser.store_id != 0 && toDate != '1000-01-01' && fromDate != '1000-01-01') {
                userDetails1 = await CustomerMaster.findAll({
                    where: {
                        store_id: misUser.store_id,
                        created_at: {
                            [op.gte]: fromDate,
                            [op.lte]: toDate
                        }
                    },
                    include: [
                        { model: Address, as: 'address' }
                    ]
                });
            }
            else if (misUser.store_id != 0 && toDate === '1000-01-01' && fromDate === '1000-01-01') {
                userDetails1 = await CustomerMaster.findAll({
                    where: {
                        store_id: misUser.store_id
                    },
                    include: [
                        { model: Address, as: 'address' }
                    ]
                });
            }
            else if (misUser.store_id === 0 && toDate != '1000-01-01' && fromDate != '1000-01-01') {
                userDetails1 = await CustomerMaster.findAll({
                    where: {
                        store_id: misUser.store_id,
                        created_at: {
                            [op.gte]: fromDate,
                            [op.lte]: toDate
                        }
                    },
                    include: [
                        { model: Address, as: 'address' }
                    ]
                });
            }
            else {
                userDetails1 = await CustomerMaster.findAll({
                    where: {
                        store_id: misUser.store_id
                    },
                    include: [
                        { model: Address, as: 'address' }
                    ]
                });
            }
            userDetails.push(...userDetails1);

        }

        if (userDetails.length > 0) {
            res.json({ status: 1, message: "customer details list ", data: userDetails })
        }
        else {
            res.json({ status: 0, message: "unable to fetch customer details list, please try again ", data: [] })
        }
    }
    catch (error) {
        console.log('\nmis_user.controller.customerList Error triggered -->');
        console.log("\nmis_user.controller.customerList Error triggered -->" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}

exports.saleOrdersList = async (req, res) => {
    console.log('\nmis_user.controller.salesOrdersList triggered -->');
    const { OrderHeader, CustomerMaster, OrderStatusMaster, Store, db1Conn } = await getModels();
    try {
        const { store_id } = req.body;
        // console.log("\nREQUESTED FROM  DATE:" + fromDate);
        // console.log("\nREQUESTED FROM  DATE:" + toDate);
        console.log("\nSTORE ID:" + store_id);
        const orders = await OrderHeader.findAll({
            where: {
                store_id: store_id,
                // order_date: {
                //     [op.between]: [fromDate, toDate]
                // }
            },
            order: [['sales_order_id', 'DESC']]
        });
        console.log(JSON.stringify(orders));
        await Promise.all(orders.map(async (element) => {
            // Select customer_name from customer_master based on customer_id found in sales_order_header
            let customer = await CustomerMaster.findOne({ where: { customer_id: element.customer_id } });

            let storeDetails = await Store.findOne({ where: { store_id: store_id } });

            let statusMaster = await OrderStatusMaster.findOne({ where: { status_id: element.status_id } });

            console.log("\n\nUSER MASTER DETAILS======", JSON.stringify(customer, 0, 2));
            if (customer) {
                element.setDataValue('customer_name', customer.customer_fname + " " + customer.customer_lname);
            }
            else {
                element.setDataValue('customer_name', 'NA');
            }

            element.setDataValue('status_name', statusMaster.status_desc);
            element.setDataValue('store_name', storeDetails.store_name);

        }));
        if (orders.length > 0) {
            res.json({ status: 1, message: "orders details ", data: orders });
        }
        else {
            res.json({ status: 0, message: "no orders details ", data: [] });
        }
    }
    catch (error) {
        console.log('\nmis_user.controller.salesOrdersList Error triggered -->');
        console.log("\nmis_user.controller.salesOrdersList Error triggered -->" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}


exports.saleOrderDetails = async (req, res) => {
    console.log('\nmis_user.controller.saleOrderDetails triggered -->');
    const { OrderHeader, OrderDetails, CustomerMaster, ProductPrice, ProductImage, OrderAddress, ProductMaster, db1Conn } = await getModels();
    try {
        const { sales_order_id, store_id } = req.body;
        console.log("\nSALES ORDER ID:" + sales_order_id);
        console.log("\nSTORE ID:" + store_id);
        const salesOrderDetails = await OrderDetails.findAll({
            where: {
                store_id: store_id,
                sales_order_id: sales_order_id
            }
        });

        let addressDetails = await OrderAddress.findOne({ where: { sales_order_id: sales_order_id } });

        await Promise.all(salesOrderDetails.map(async (element) => {
            const productDetails = await ProductMaster.findOne({
                where: {
                    prod_id: element.prod_id
                }
            });

            const productPriceDetails = await ProductPrice.findOne({
                where: {
                    store_id: store_id,
                    prod_id: element.prod_id
                }
            });

            const imageDetails = await ProductImage.findOne({
                where: {
                    prod_id: element.prod_id
                }
            });

            if (imageDetails.prod_image_path == null) {
                element.setDataValue('product_image', '');
            } else {
                element.setDataValue('product_image', imageDetails.prod_image_path);
            }
            element.setDataValue('productPrice', productPriceDetails.prod_price);
            element.setDataValue('product_name', productDetails.prod_name);
        }));

        let finalData = { 'orderDetails': salesOrderDetails, 'address': addressDetails };

        if (salesOrderDetails) {
            res.json({ status: 1, message: "order details ", data: finalData });
        }
        else {
            res.json({ status: 0, message: "no order details ", data: [] });
        }
    }
    catch (error) {
        console.log('\nmis_user.controller.saleOrderDetails Error triggered -->');
        console.log("\nmis_user.controller.saleOrderDetails Error triggered -->" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}


exports.getStoreNextOrderStatusList = async (req, res) => {
    console.log('\nmis_user.controller.getStoreNextOrderStatusList triggered -->');
    const { OrderStatusMaster, OrderHeader, OrderDetails, CustomerMaster, db1Conn } = await getModels();
    try {
        const { store_id, status_id } = req.body;
        console.log("\nSTORE ID:" + store_id);
        console.log("\nSTATUS ID:" + status_id);
        const orderStatusList = await OrderStatusMaster.findAll({
            where: {
                store_id: store_id,
                status_id: {
                    [op.gt]: status_id
                }
            }
        });

        if (orderStatusList.length > 0) {
            res.json({ status: 1, message: "order status list ", data: orderStatusList });
        }
        else {
            res.json({ status: 0, message: "empty order status list", data: [] });
        }
    }
    catch (error) {
        console.log('\nmis_user.controller.getStoreNextOrderStatusList Error triggered -->');
        console.log("\nmis_user.controller.getStoreNextOrderStatusList Error triggered -->" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "order status list error", data: JSON.stringify(error, 0, 2) });
    }
}

exports.saleOrderStatusUpdate = async (req, res) => {
    console.log('\nmis_user.controller.saleOrderStatusUpdate triggered -->');
    const { OrderStatusMaster, OrderHeader, OrderDetails, CustomerMaster, db1Conn } = await getModels();
    try {
        const { sales_order_id, store_id, status_id } = req.body;
        console.log("\nSALES ORDER ID:" + sales_order_id);
        console.log("\nSTORE ID:" + store_id);
        console.log("\nSTATUS ID:" + status_id);
        // const orderStatus = await OrderStatusMaster.findAll({
        //     where: {
        //         store_id: store_id,
        //         status_id: {
        //             [op.gt]:status_id
        //         }
        //     }
        // });
        await OrderHeader.update(
            { status_id: status_id },
            { where: { sales_order_id: sales_order_id, store_id: store_id } }
        )

        let salesOrderHeader = await OrderHeader.findOne({ where: { sales_order_id: sales_order_id, store_id: store_id } });
        if (salesOrderHeader) {
            res.json({ status: 1, message: "order status update successfully ", data: salesOrderHeader });
        }
        else {
            res.json({ status: 0, message: "order status not updated, please try again ", data: {} });
        }
    }
    catch (error) {
        console.log('\nmis_user.controller.saleOrderStatusUpdate Error triggered -->');
        console.log("\nmis_user.controller.saleOrderStatusUpdate Error triggered -->" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "order status update error", data: JSON.stringify(error, 0, 2) });
    }
}

exports.productAdd = async (req, res) => {
    console.log('Product Add controller Triggered');
    const { ProductMaster, ProductImage, ProductPrice, ProductStock, ProductStockMove } = await getModels();
    const reqbody = req.body;
    if (reqbody.length > 0) {
        await Promise.all(reqbody.map(async (element) => {
            const productHeaderDetails = await ProductMaster.create(
                {
                    prod_code: '51540',
                    prod_name: element.prod_name,
                    prod_desc: 'vegetable',
                    barcode: '56165151',
                    manufacturer_id: 1,
                    sku: element.uom,
                    sku_type: '561561',
                    prod_category_id: element.category_id,
                    parent_id: 0
                }
            );
            console.log(JSON.stringify(productHeaderDetails));
            const productImageDetails = await ProductImage.create({
                prod_id: productHeaderDetails.prod_id,
                prod_image_path: 'dfbsdbfgb'
            });
            console.log(JSON.stringify(productImageDetails));
            const productPriceDetails = await ProductPrice.create({
                prod_id: productHeaderDetails.prod_id,
                prod_price: element.prod_price,
                prod_status: 1,
                vendor: 0,
                delivery: 0,
                company: 0,
                store_id: element.store_id
            });
            console.log(JSON.stringify(productPriceDetails));
            const productStockDetails = await ProductStock.create({
                store_id: element.store_id,
                sale_point_id: 1,
                current_stock: 0,
                sku: element.uom,
                prod_id: productHeaderDetails.prod_id
            });
            console.log(JSON.stringify(productStockDetails));
            // const productStockMoveDetails = await ProductStockMove.create({
            //     store_id: element.store_id,
            //     product_id: productHeaderDetails.prod_id,
            //     mov_type: 1,
            //     mov_quantity: element.prod_stock,
            //     mov_sku: 1,
            //     sale_point_id: 0,
            //     sales_date: 0,
            //     sales_time: 0,
            //     close_stock: 0,
            //     close_stock_sku: 0,
            //     open_stock: 0,
            //     open_stock_sku: 0
            // });
            // console.log(JSON.stringify(productStockMoveDetails));
        }));
        res.json({ status: 1, message: "Product add successfully.", data: reqbody });
    } else {
        res.json({ status: 0, message: "Select atleast one product." });
    }
}

exports.storeList = async (req, res) => {
    console.log('Store List Triggered');
    const { Store } = await getModels();
    try {
        const storeLists = await Store.findAll({ attributes: ['store_id', 'store_name', 'store_description'] });
        res.json({ status: 1, message: "Store List", data: storeLists });
    } catch (error) {
        res.json({ status: 0, message: "Store list not found", data: [] });
    }
}

exports.productList = async (req, res) => {
    console.log('Product List controller triggered');
    const reqbody = req.body;
    const { ProductMaster, ProductPrice, Uom, ProductImage, ProductStock } = await getModels();
    try {
        const productLists = await ProductMaster.findAll({
            include: [{ model: ProductPrice, where: { store_id: reqbody.store_id } },
            { model: ProductImage, as: 'image_details' },
            { model: ProductStock, as: 'stock_details', where: { store_id: reqbody.store_id } }
            ]
        });
        await Promise.all(productLists.map(async (element) => {
            const uomDetails = await Uom.findOne({
                where: { id: element.stock_details.sku }
            })
            element.setDataValue('uomDetails', uomDetails.uom_desc);
        }))

        res.json({ status: 1, message: "Product List", data: productLists });
    } catch (error) {
        res.json({ status: 0, message: "Product Not Found", data: [] });
        console.log(error);
    }
}

exports.uomList = async (req, res) => {
    console.log('uomList controller triggered');
    const { Uom } = await getModels();
    try {
        const uomdetails = await Uom.findAll();
        res.json({ status: 1, message: 'Uom Found', data: uomdetails });
    } catch (error) {
        console.log(error);
        res.json({ status: 0, message: 'Uom Not Found', data: [] });
    }
}


