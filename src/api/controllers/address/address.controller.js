const moment = require('moment');
// local imports
const { getModels } = require('../../models');

exports.addAddress = async (req, res) => {
    const { Address, db1Conn } = await getModels();
    console.log('\naddress.controller.addAddress  triggered -->');
    console.log(req.body.customer_id + ' ' + req.body.phone_no);
    try {
        const address = await Address.create(
            {
                address1: req.body.address.address1, address2: req.body.address.address2, address3: req.body.address.address3,
                city: req.body.address.city, state: req.body.address.state, pincode: req.body.address.pincode,
                longitude: req.body.address.longitude, latitude: req.body.address.latitude, flag_default: req.body.address.flag_default, customer_id: req.body.address.customer_id, phone_no: req.body.address.phone_no
            },
        );
        console.log("\n\n ADDRESS ADDED ======", JSON.stringify(address, 0, 2));
        res.json({ status: 1, message: "address added successfully", data: address })
    }
    catch (error) {
        console.log("address.controller.addAddress ERROR==" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}

exports.editAddress = async (req, res) => {
    const { Address, db1Conn } = await getModels();
    console.log('\naddress.controller.editAddress  triggered -->');

    try {
        let address = await Address.update({
            address1: req.body.address1, address2: req.body.address2, address3: req.body.address3,
            city: req.body.city, state: req.body.state, pincode: req.body.pincode,
            longitude: req.body.longitude, latitude: req.body.latitude, flag_default: req.body.flag_default, phone_no: req.body.phone_no
        }, { where: { address_id: req.body.address_id } });
        console.log("\n\n  ADDRESS UPDATED ======", JSON.stringify(address, 0, 2));
        res.json({ status: 1, message: "address updated successfully", data: address })
    }
    catch (error) {
        console.log("address.controller.editAddress ERROR==" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}

exports.deleteAddress = async (req, res) => {
    console.log('\naddress.controller.deleteAddress  triggered -->');
    const { Address, db1Conn } = await getModels();
    try {
        let address = await Address.destroy({ where: { address_id: req.body.address_id, customer_id: req.body.customer_id } });
        console.log("\n\n ADDRESS DELETED ======", JSON.stringify(address, 0, 2));
        console.log("\n\n ADDRESS DELETED SUCCESSFULLY ")
        res.json({ status: 1, message: "address deleted successfully", data: address })
    }
    catch (error) {
        console.log("address.controller.deleteAddress ERROR==" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}


exports.addressList = async (req, res) => {
    console.log('\naddress.controller.addressList  triggered -->');
    const { Address } = await getModels();
    try {
        const addressList = await Address.findAll({ where: { customer_id: req.params.customer_id } });
        if (addressList.length > 0) {
            console.log("\n\n ADDRESS LIST======", JSON.stringify(addressList, 0, 2));
            res.json({ status: 1, message: "address list ", data: addressList });
        }
        else {
            console.log("\n\n EMPTY ADDRESS LIST======", JSON.stringify(addressList, 0, 2));
            res.json({ status: 1, message: "empty address list ", data: addressList });
        }
    }
    catch (error) {
        console.log("\n\naddress.controller.addressList ERROR==" + error);
        // res.status(404).send(error);
        res.json({ status: 0, message: "error", data: JSON.stringify(error, 0, 2) });
    }
}